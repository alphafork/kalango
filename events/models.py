from django.db import models


class Event(models.Model):
    class Meta:
        unique_together = ('event_id', 'event_year')

    EVENT_TYPE_CHOICES = [
        ('Vara', 'Vara'),
        ('Rachana', 'Rachana'),
    ]

    EVENT_TYPE_ML_CHOICES = [
        ('വര', 'വര'),
        ('രചന', 'രചന'),
    ]

    EVENT_LANG_CHOICES = [
        ('മലയാളം', 'മലയാളം'),
        ('ഹിന്ദി', 'ഹിന്ദി'),
        ('ഹിന്ദി', 'ഹിന്ദി'),
        ('ഇംഗ്ലീഷ്', 'ഇംഗ്ലീഷ്'),
        ('അറബി', 'അറബി'),
        ('തമിഴ്', 'തമിഴ്'),
        ('കന്നഡ', 'കന്നഡ'),
        ('സംസ്കൃതം', 'സംസ്കൃതം'),
    ]

    SCHOOL_LEVEL_CHOICES = [
        ('HS', 'High School'),
        ('HSS', 'Higher Secondary School'),
    ]

    SCHOOL_LEVEL_ML_CHOICES = [
        ('എച്ച്.എസ്', 'എച്ച്.എസ്'),
        ('എച്ച്.എസ്.എസ്', 'എച്ച്.എസ്.എസ്'),
    ]

    RESULT_STATUS_CHOICES = [
        ('result_unavailable', 'Result unavailable'),
        ('result_available', 'Result available'),
        ('result_scraped', 'Result scraped'),
        ('rename_script_generated', 'Rename script generated'),
        ('wiki_updated', 'Wiki updated'),
    ]

    event_id = models.IntegerField('Event ID')
    event_name = models.CharField('Event Name', max_length=200)
    event_name_ml = models.CharField('Event Name (Malayalam)', max_length=300)
    event_type = models.CharField('Event Type', max_length=100)
    event_type_ml = models.CharField('Event Type (Malayalam)', max_length=200, choices=EVENT_TYPE_ML_CHOICES)
    event_lang = models.CharField('Event Language', max_length=100, null=True, blank=True, choices=EVENT_LANG_CHOICES)
    event_year = models.PositiveSmallIntegerField('Event Year', null=True)
    school_level = models.CharField('School Level', max_length=100, choices=SCHOOL_LEVEL_CHOICES, default='HS')
    school_level_ml = models.CharField('School Level (Malayalam)', max_length=100, choices=SCHOOL_LEVEL_ML_CHOICES)
    result_status = models.CharField('Result Status', max_length=100, choices=RESULT_STATUS_CHOICES, default='result_unavailable')

    def __str__(self):
        return self.event_name
