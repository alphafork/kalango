from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from skscraper import scraper


def scrape_events():
    event_model = apps.get_model(settings.EVENT_MODEL)
    events = scraper.getEvents()
    for event in events:
        print(event)
        existing_event = event_model.objects.filter(
            event_id=event['eventid'],
            event_year=int(settings.KALOLSAVAM_YEAR) - 1
        )
        if (existing_event.exists()):
            ev = existing_event.values()[0]
            event_category = event.get('eventcategory', None)
            if (event_category == 'HS General'):
                school_level_ml = 'എച്ച്എസ് ജനറൽ'
            elif (event_category == 'HSS General'):
                school_level_ml = 'എച്ച്എസ്എസ് ജനറൽ'
            elif (event_category == 'HS Arabic'):
                school_level_ml = 'എച്ച്എസ് അറബിക്'
            elif (event_category == 'HS Sanskrit'):
                school_level_ml = 'എച്ച്എസ് സംസ്കൃതം'
            else:
                school_level_ml = ''

            published_time = event.get('publishedtime', None)
            if (published_time):
                ev.update({'result_status': 'result_available'})

            ev.update({
                'event_name': event['eventname'],
                'event_year': settings.KALOLSAVAM_YEAR,
                'school_level': event['eventcategory'],
                'school_level_ml': school_level_ml,
            })
            ev.pop('id', None)

            event_model.objects.update_or_create(
                event_id=event['eventid'],
                event_year=settings.KALOLSAVAM_YEAR,
                defaults=ev,
            )
        else:
            print('New event: ', event)


def get_events(event_type='all'):
    event_model = apps.get_model(settings.EVENT_MODEL)
    keys = {'event_year': settings.KALOLSAVAM_YEAR}

    if (event_type != 'all'):
        keys['event_type'] = event_type

    events = event_model.objects.filter(
        **keys
    ).values_list('event_id', flat=True)

    return events


def scrape_results(events=[]):
    if (not events):
        events = get_events()
        print(events)

    for event_id in events:
        event_model = apps.get_model(settings.EVENT_MODEL)
        result_model = apps.get_model(settings.RESULT_MODEL)
        try:
            event = event_model.objects.exclude(
                result_status='result_unavailable'
            ).get(
                event_id=event_id,
                event_year=settings.KALOLSAVAM_YEAR,
            )
        except ObjectDoesNotExist:
            print('Event', event_id, 'doesn\'t exist, try again.')

        results = scraper.getEventResult(event_id)
        print(results)
        if (not results):
            continue
        for result in results:
            print(result)
            result['event'] = event
            result_model.objects.update_or_create(
                event=event,
                codeno=result['codeno'],
                defaults=result,
            )

        event = event_model.objects.filter(
            event_id=event_id,
            event_year=settings.KALOLSAVAM_YEAR,
        ).update(result_status='result_scraped')


def scrape_results_list():
    scraper.fetchAll()
