"""
Ref: https://github.com/wikimedia/mediawiki-api-demos
"""

from django.conf import settings
import requests


def edit_wiki(title, **kwargs):
    S = requests.Session()

    URL = settings.WIKI_URL

    # Step 1: GET Request to fetch login token
    PARAMS_0 = {
        "action": "query",
        "meta": "tokens",
        "type": "login",
        "format": "json"
    }

    R = S.get(url=URL, params=PARAMS_0)
    DATA = R.json()

    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

    # Step 2: POST Request to log in. Use of main account for login is not
    # supported. Obtain credentials via Special:BotPasswords
    # (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword
    PARAMS_1 = {
        "action": "login",
        "lgname": settings.WIKI_LGNAME,
        "lgpassword": settings.WIKI_LGPASSWORD,
        "lgtoken": LOGIN_TOKEN,
        "format": "json"
    }

    R = S.post(URL, data=PARAMS_1)

    # Step 3: GET request to fetch CSRF token
    PARAMS_2 = {
        "action": "query",
        "meta": "tokens",
        "format": "json"
    }

    R = S.get(url=URL, params=PARAMS_2)
    DATA = R.json()

    CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

    # Step 4: POST request to edit a page
    PARAMS_3 = {
        "action": "edit",
        "title": title,
        "token": CSRF_TOKEN,
        "format": "json",
        **kwargs,
    }

    R = S.post(URL, data=PARAMS_3)
    DATA = R.json()

    return DATA


def get_redirects(title):
    S = requests.Session()
    URL = settings.WIKI_URL

    PARAMS = {
        "action": "query",
        "format": "json",
        "titles": title,
        "redirects": 1,
    }

    R = S.get(url=URL, params=PARAMS)
    DATA = R.json()

    PAGES = DATA["query"]["pages"]

    return list(PAGES.values())[0]['title']
