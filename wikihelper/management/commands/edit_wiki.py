from django.core.management.base import BaseCommand
from wikihelper.services import (
    write_results,
    write_results_list,
    write_school_results,
)


class Command(BaseCommand):
    help = 'Create Wiki Article based on the given Wiki Template'

    def add_arguments(self, parser):
        parser.add_argument(
            '--results',
            action='extend',
            nargs='+',
            type=int,
        )
        parser.add_argument(
            '--results-list',
            action='store_true',
        )
        parser.add_argument(
            '--school-results',
            action='store_true',
        )

    def handle(self, *args, **options):
        if (options['results']):
            write_results(options['results'])

        if (options['results_list']):
            write_results_list()

        if (options['school_results']):
            write_school_results()
