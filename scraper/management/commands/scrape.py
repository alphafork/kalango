from django.core.management.base import BaseCommand
from scraper.services import (
    get_events,
    scrape_events,
    scrape_results,
    scrape_results_list,
)


class Command(BaseCommand):
    help = 'Scrape Kerala State School Kalolsavam results from KITE website'

    def add_arguments(self, parser):
        parser.add_argument(
            '--all-events',
            action='store_true',
        )
        parser.add_argument(
            '--results',
            action='extend',
            nargs='+',
            type=int,
        )
        parser.add_argument(
            '--all-results',
            action='store_true',
        )
        parser.add_argument(
            '--vara-rachana-results',
            action='store_true',
        )
        parser.add_argument(
            '--all-results-list',
            action='store_true',
        )

    def handle(self, *args, **options):
        if (options['all_events']):
            scrape_events()

        if (options['results']):
            print('Scraping all events...\n')
            scrape_events()
            print(
                '\n\n-------------\n\n',
                'Scraping results...\n',
            )
            scrape_results(options['results'])

        if (options['all_results']):
            print('Scraping all events...\n')
            scrape_events()
            print(
                '\n\n-----------------\n\n',
                'Scraping all results...\n',
            )
            scrape_results()

        if (options['vara_rachana_results']):
            vara_events = get_events('vara')
            rachana_events = get_events('rachana')
            scrape_results(vara_events)
            scrape_results(rachana_events)

        if (options['all_results_list']):
            print('Scraping list of all results...\n')
            scrape_results_list()
