from django.db import models
from django.conf import settings


class Result(models.Model):
    # Field names chosen to keep backward compatibility with old CI app
    no = models.IntegerField('Grade-wise Serial No.')
    regno = models.CharField('Registration No.', max_length=10)
    codeno = models.IntegerField('Code No.')
    studentname = models.CharField('Student Name', max_length=200)
    standard = models.IntegerField('Standard (Class)')
    grade = models.CharField('Grade', max_length=2)
    point = models.CharField('Point', max_length=4, null=True, blank=True)
    remarks = models.TextField('Remarks', max_length=500, null=True, blank=True)
    schoolid = models.IntegerField('School ID')
    schoolname = models.CharField('School Name', max_length=200)
    event = models.ForeignKey(settings.EVENT_MODEL, on_delete=models.CASCADE, verbose_name='Event')

    class Meta:
        constraints = [models.UniqueConstraint(name="unique_baseid", fields=['regno', 'event'])]

    @property
    def schoolidandname(self):
        return str(self.schoolid) + ' - ' + self.schoolname

    @property
    def baseid(self):
        return self.regno + str(self.event_id)
