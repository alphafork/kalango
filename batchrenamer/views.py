from django.shortcuts import render
from django.conf import settings
from events.models import Event
from results.models import Result
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
import json


@ensure_csrf_cookie
def rename_script(request):
    if request.method == "GET":
        context = {}
        context['results'] = Result.objects.filter(
            event__event_year=settings.KALOLSAVAM_YEAR,
            event__event_type__in=['Vara', 'Rachana']
        ).exclude(
            event__result_status='result_unavailable',
        )
        context['ssk_year'] = settings.KALOLSAVAM_ACADEMIC_YEAR
        context['ssk_no'] = settings.KALOLSAVAM_NO
        return render(request, "command_list.html", context)
    if request.method == "POST":
        Event.objects.filter(
            event_year=settings.KALOLSAVAM_YEAR,
            event_id=json.loads(request.body)["event_id"],
        ).update(result_status="rename_script_generated")
        return JsonResponse(data={"success": True}, status=200)
