from django.db import models


class WikiTemplate(models.Model):
    template_name = models.CharField('Template Name', max_length=100)
    template = models.TextField('Template')
    template_year = models.IntegerField('Year created')
    template_remarks = models.TextField('Remarks', null=True, blank=True)

    def __str__(self):
        return self.template_name
