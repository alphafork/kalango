from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from string import Template
from wikihelper.models import WikiTemplate
from wikihelper.utils import edit_wiki, get_redirects


def get_event(event_id):
    event_model = apps.get_model(settings.EVENT_MODEL)
    year = settings.KALOLSAVAM_YEAR

    try:
        event = event_model.objects.get(
            event_id=event_id,
            event_year=year,
        )
    except ObjectDoesNotExist:
        print('Event', event_id, 'doesn\'t exist, try again')
    else:
        return event


def get_event_name(event):
    event_name_ml = event.event_name_ml
    if (event.event_lang):
        event_name_ml = event.event_lang + ' ' + event.event_name_ml
    return event_name_ml


def get_confirmation(message):
    confirm = 'Y'
    confirm = input(''.join(message)) or 'y'
    if (confirm != 'y' and confirm != 'Y'):
        return False
    return True


def set_event_type_template(events, event_type):
    events_by_type = events[event_type]
    year = settings.KALOLSAVAM_YEAR

    vara_list_template = WikiTemplate.objects.get(
        template_name='Vara List',
        template_year=year,
    )

    rachana_list_template = WikiTemplate.objects.get(
        template_name='Rachana List',
        template_year=year,
    )

    templates = {
        'vara': vara_list_template,
        'rachana': rachana_list_template,
    }

    for school_level in events[event_type]:
        event_list = []

        for event_by_type in events_by_type[school_level]['events']:
            grade_list = []

            for grade in event_by_type['grades']:
                if (grade == 'A'):
                    grade_list.append('G1=A')
                if (grade == 'B'):
                    grade_list.append('G2=B')
                if (grade == 'C'):
                    grade_list.append('G3=C')
                if (grade == 'NG'):
                    grade_list.append('G4=NG')

            grades = '|'.join(grade_list)
            event = event_by_type['data']
            event_name_ml = get_event_name(event)

            event_list_content = Template(
                templates[event_type].template
            ).substitute(
                event_id=event_by_type['data'].event_id,
                event_name_ml=event_name_ml,
                school_level=event_by_type['data'].school_level,
                year=settings.KALOLSAVAM_ACADEMIC_YEAR,
                grades=grades,
            )
            event_list.append(event_list_content)

        template = '\n'.join(event_list)
        events_by_type[school_level]['template'] = template
        events[event_type] = events_by_type

    return events


def set_event_type_content(events, event_type):
    events_by_type = events[event_type]
    year = settings.KALOLSAVAM_YEAR

    vara_list_wrapper_template = WikiTemplate.objects.get(
        template_name='Vara List Wrapper',
        template_year=year,
    )
    rachana_list_wrapper_template = WikiTemplate.objects.get(
        template_name='Rachana List Wrapper',
        template_year=year,
    )
    templates = {
        'vara': vara_list_wrapper_template,
        'rachana': rachana_list_wrapper_template,
    }

    for school_level in events_by_type:
        event_list_wrapper_content = Template(
            templates[event_type].template
        ).substitute(
            school_level=school_level,
            results_by_category=events_by_type[school_level]['template'],
        )

        events_by_type[school_level]['wrapper_template'] = event_list_wrapper_content
        events[event_type] = events_by_type

    return events


def set_event_type_url(events, event_type):
    events_by_type = events[event_type]
    year = settings.KALOLSAVAM_YEAR

    vara_list_url_template = WikiTemplate.objects.get(
        template_name='Vara List URL',
        template_year=year,
    )
    rachana_list_url_template = WikiTemplate.objects.get(
        template_name='Rachana List URL',
        template_year=year,
    )
    templates = {
        'vara': vara_list_url_template,
        'rachana': rachana_list_url_template,
    }

    for school_level in events_by_type:
        event_list_url = Template(
            templates[event_type].template
        ).substitute(
            year=settings.KALOLSAVAM_ACADEMIC_YEAR,
            school_level=school_level,
        )

        events_by_type[school_level]['url'] = event_list_url
        events[event_type] = events_by_type

    return events


def write_results(events):
    result_model = apps.get_model(settings.RESULT_MODEL)
    event_model = apps.get_model(settings.EVENT_MODEL)
    year = settings.KALOLSAVAM_YEAR
    academic_year = settings.KALOLSAVAM_ACADEMIC_YEAR

    for event_id in events:
        event = get_event(event_id)
        confirmed = True

        if (event.result_status != 'rename_script_generated'):
            message = (
                'You haven\'t run rename script for the event ',
                str(event_id),
                '. Do you want to continue? [Y/n].',
            )
            confirmed = get_confirmation(message)

        if (not confirmed):
            print('Skipping event ', event_id, '...')
            continue

        if (event.result_status == 'wiki_updated'):
            message = (
                'Result of the event ',
                str(event_id),
                ' already published to wiki. ',
                'Do you want to overwrite? [Y/n]: '
            )
            confirmed = get_confirmation(message)

        if (not confirmed):
            print('Skipping event ', event_id, '...')
            continue

        individual_result_template = WikiTemplate.objects.get(
            template_name=event.event_type,
            template_year=year,
        )
        individual_result_url_template = WikiTemplate.objects.get(
            template_name='Individual Result URL',
            template_year=year,
        )

        list_result_wrapper_template = WikiTemplate.objects.get(
            template_name='Grade List Wrapper',
            template_year=year,
        )
        list_result_template = WikiTemplate.objects.get(
            template_name='Grade',
            template_year=year,
        )
        list_result_url_template = WikiTemplate.objects.get(
            template_name='Grade List URL',
            template_year=year,
        )

        errors = 0
        list_result_content = {}
        event_name_ml = get_event_name(event)
        event_name_general = event.event_name_ml
        school_level = event.school_level
        results = result_model.objects.filter(event=event)

        distinct_grades = results.values_list(
            'grade',
            flat=True,
        ).distinct()

        for distinct_grade in distinct_grades:
            i = 0
            list_result_content[distinct_grade] = []

            for result in results:
                grade = result.grade
                grade_slug = 'മറ്റുള്ളവ' if grade == 'NG' else grade + '_ഗ്രേഡ്'
                if (grade != distinct_grade):
                    continue

                studentname = result.studentname
                standard = result.standard
                schoolidandname = result.schoolidandname
                schoolid = result.schoolid
                codeno = result.codeno
                i += 1
                no = str(i).zfill(2)

                individual_result_content = Template(
                    individual_result_template.template
                ).substitute(
                    studentname=studentname,
                    standard=standard,
                    year=academic_year,
                    schoolidandname=schoolidandname,
                    schoolid=schoolid,
                    event_name_ml=event_name_ml,
                    school_level=school_level,
                    event_id=event_id,
                    grade=grade,
                    codeno=codeno,
                )

                grade_list_content = Template(
                    list_result_template.template
                ).substitute(
                    year=academic_year,
                    event_name_ml=event_name_ml,
                    school_level=school_level,
                    grade_slug=grade_slug,
                    no=no,
                )

                list_result_content[grade].append(
                    grade_list_content
                )

                individual_result_url = Template(
                    individual_result_url_template.template
                ).substitute(
                    year=academic_year,
                    event_name_ml=event_name_ml,
                    school_level=school_level,
                    grade_slug=grade_slug,
                    no=no,
                )

                response = edit_wiki(
                    individual_result_url,
                    text=individual_result_content,
                )
                print(response)
                if response['edit']['result'] != 'Success':
                    errors += 1

        for grade in distinct_grades:
            results_by_grade = ' '.join(list_result_content[grade])
            grade_slug = 'മറ്റുള്ളവ' if grade == 'NG' else grade + ' ഗ്രേഡ്'
            list_result_wrapper_content = Template(
                list_result_wrapper_template.template
            ).substitute(
                event_name_general=event_name_general,
                event_name_ml=event_name_ml,
                year=academic_year,
                school_level=school_level,
                grade_slug=grade_slug,
                results_by_grade=results_by_grade,
            )

            list_result_url = Template(
                list_result_url_template.template
            ).substitute(
                year=academic_year,
                event_name_ml=event_name_ml,
                school_level=school_level,
                grade_slug=grade_slug,
            )

            response = edit_wiki(
                list_result_url,
                text=list_result_wrapper_content,
            )
            print(response)
            if response['edit']['result'] != 'Success':
                errors += 1

        if (errors == 0):
            event.result_status = 'wiki_updated'
            event.save()

        published_events = event_model.objects.filter(
            event_year=year,
        ).exclude(
            result_status='result_unavailable'
        ).order_by('event_id')

        if (not published_events):
            continue

        events_to_update = {}
        events_to_update['vara'] = {}
        events_to_update['rachana'] = {}
        vara_events = events_to_update['vara']
        rachana_events = events_to_update['rachana']

        events = event_model.objects.filter(
            event_type__in=['Vara', 'Rachana']
        ).filter(event_year=year)

        distinct_school_levels = events.values_list(
            'school_level',
            flat=True,
        ).distinct()

        for distinct_school_level in distinct_school_levels:
            if not distinct_school_level:
                continue

            vara_events[distinct_school_level] = {}
            vara_events[distinct_school_level]['events'] = []
            rachana_events[distinct_school_level] = {}
            rachana_events[distinct_school_level]['events'] = []

        for published_event in published_events:
            if not published_event.school_level:
                continue

            results = result_model.objects.filter(event=published_event).order_by('grade')
            distinct_grades = results.values_list('grade', flat=True).distinct()

            if (published_event.event_type == 'Vara'):
                vara_events[published_event.school_level]['events'].append({
                    'data': published_event,
                    'grades': distinct_grades,
                })

            if (published_event.event_type == 'Rachana'):
                rachana_events[published_event.school_level]['events'].append({
                    'data': published_event,
                    'grades': distinct_grades,
                })

        for event_type in events_to_update:
            events_to_update = set_event_type_template(
                events_to_update,
                event_type,
            )
            events_to_update = set_event_type_content(
                events_to_update,
                event_type,
            )
            events_to_update = set_event_type_url(
                events_to_update,
                event_type,
            )

            for school_level in events_to_update[event_type].values():
                if (school_level['template']):
                    response = edit_wiki(
                        school_level['url'],
                        text=school_level['wrapper_template'],
                    )
                    print(response)


def write_results_list():
    event_model = apps.get_model(settings.EVENT_MODEL)
    result_model = apps.get_model(settings.RESULT_MODEL)
    year = settings.KALOLSAVAM_YEAR
    events = event_model.objects.filter(
        event_year=year
    ).exclude(
        result_status='result_unavailable'
    ).order_by('event_id')

    distinct_school_levels = events.values_list(
        'school_level',
        flat=True,
    ).distinct()

    individual_result_row_template = WikiTemplate.objects.get(
        template_name='Individual Result Row',
        template_year=year,
    )

    individual_result_table_wrapper_template = WikiTemplate.objects.get(
        template_name='Individual Result Table Wrapper',
        template_year=year,
    )

    individual_result_table_url_template = WikiTemplate.objects.get(
        template_name='Individual Result Table URL',
        template_year=year,
    )

    result_row_template = WikiTemplate.objects.get(
        template_name='Result Row',
        template_year=year,
    )

    result_list_wrapper_template = WikiTemplate.objects.get(
        template_name='Result List Wrapper',
        template_year=year,
    )

    result_list_url_template = WikiTemplate.objects.get(
        template_name='Result List URL',
        template_year=year,
    )

    events_to_update = {}
    for event in events:
        results = result_model.objects.filter(event=event)
        individual_result_rows = []

        for result in results:
            individual_result_row_content = Template(
                individual_result_row_template.template
            ).substitute(
                regno=result.regno,
                codeno=result.codeno,
                studentname=result.studentname,
                standard=result.standard,
                schoolid=result.schoolid,
                schoolname=result.schoolname,
                grade=result.grade,
                point=result.point,
                remarks=result.remarks,
            )
            individual_result_rows.append(individual_result_row_content)

        result_rows = '\n'.join(individual_result_rows)

        individual_result_table_wrapper_content = Template(
            individual_result_table_wrapper_template.template
        ).substitute(
            event_id=event.event_id,
            event_name=event.event_name,
            school_level=event.school_level,
            result_rows=result_rows,
        )

        individual_result_table_url = Template(
            individual_result_table_url_template.template
        ).substitute(
            school_level=event.school_level,
            event_id=event.event_id,
        )

        response = edit_wiki(
            individual_result_table_url,
            text=individual_result_table_wrapper_content,
        )
        print(response)

    for school_level in distinct_school_levels:
        events_to_update[school_level] = {}
        events_to_update[school_level]['template'] = []

        for event in events:
            if (event.school_level == school_level):
                result_row_content = Template(
                    result_row_template.template
                ).substitute(
                    school_level=school_level,
                    event_id=event.event_id,
                    event_name=event.event_name,
                )
                events_to_update[school_level]['template'].append(
                    result_row_content
                )

    print('\n\n')
    for school_level in events_to_update:
        template = events_to_update[school_level]['template']
        event_rows = '\n'.join(template)

        result_list_wrapper_content = Template(
            result_list_wrapper_template.template
        ).substitute(
            school_level=school_level,
            event_rows=event_rows,
        )

        result_list_url = Template(
            result_list_url_template.template
        ).substitute(
            school_level=school_level
        )

        response = edit_wiki(
            result_list_url,
            text=result_list_wrapper_content,
        )
        print(response)


def get_school(school_code):
    school_model = apps.get_model(settings.SCHOOL_MODEL)
    school = school_model.objects.get(school_code=school_code)
    return school


def write_school_results():
    year = settings.KALOLSAVAM_YEAR
    result_model = apps.get_model(settings.RESULT_MODEL)
    distinct_schools = result_model.objects.filter(
        event__event_year=year,
    ).values_list(
        'schoolid',
        flat=True,
    ).distinct()

    school_result_row_template = WikiTemplate.objects.get(
        template_name='School Result Row',
        template_year=year,
    )

    school_result_table_wrapper_template = WikiTemplate.objects.get(
        template_name='School Result Table Wrapper',
        template_year=year,
    )

    results_to_update = {}
    for school_code in distinct_schools:
        school_results = result_model.objects.filter(
            schoolid=school_code,
            event__event_year=year,
        )

        school_result_rows = []
        school_offstage_result_rows = []
        for result in school_results:
            school_result_row_content = Template(
                school_result_row_template.template
            ).substitute(
                regno=result.regno,
                codeno=result.codeno,
                studentname=result.studentname,
                standard=result.standard,
                eventid=result.event.event_id,
                eventname=result.event.event_name,
                grade=result.grade,
                point=result.point,
                remarks=result.remarks,
            )
            school_result_rows.append(school_result_row_content)

            if (result.event.event_type in ['Vara', 'Rachana']):
                individual_result_template = WikiTemplate.objects.get(
                    template_name=result.event.event_type,
                    template_year=year,
                )
                individual_result_content = Template(
                    individual_result_template.template
                ).substitute(
                    studentname=result.studentname,
                    standard=result.standard,
                    year=settings.KALOLSAVAM_ACADEMIC_YEAR,
                    schoolidandname=result.schoolidandname,
                    schoolid=result.schoolid,
                    event_name_ml=result.event.event_name_ml,
                    school_level=result.event.school_level,
                    event_id=result.event.event_id,
                    grade=result.grade,
                    codeno=result.codeno,
                )
                school_offstage_result_rows.append(individual_result_content)

        result_rows = '\n'.join(school_result_rows)
        offstage_result_rows = '\n'.join(school_offstage_result_rows)

        school_result_table_wrapper_content = Template(
            school_result_table_wrapper_template.template
        ).substitute(
            result_rows=result_rows,
        )

        school_name = get_redirects(school_code)
        school_result = offstage_result_rows + '\n\n' + school_result_table_wrapper_content
        school_result_url = school_name + '/സംസ്ഥാന സ്കൂൾ കലോത്സവം/' + settings.KALOLSAVAM_ACADEMIC_YEAR

        response = edit_wiki(
            school_result_url,
            text=school_result,
        )
        print(response)
