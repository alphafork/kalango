from django.apps import AppConfig


class WikihelperConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "wikihelper"
